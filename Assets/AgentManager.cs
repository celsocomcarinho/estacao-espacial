﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    GameObject[] agents;
    
    void Start()
    {   
        //encontra todos os agents com tag "ai" na scene
        agents = GameObject.FindGameObjectsWithTag("ai");
    }

    void Update()
    {
        //quando clica com o mouse no mapa, todos os agents vão até esse ponto
        if (Input.GetMouseButtonDown(0)){
            RaycastHit hit;
            if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100)){
                foreach (GameObject a in agents)
                    a.GetComponent<AIControl>().agent.SetDestination(hit.point);
            }
        }
    }
}
